package cl.grebolledoa.misactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btCambiarActividadOnClick(View v){
        //cambiarme de actividad

        //creamos un intent definiendo el contexto y la clase de la actividad a la cual nos vamos
        //a cambiar
        Intent intent = new Intent(this, OtherActivity.class);

        //le asigno al intent una variable para pasarla por parametro a mi nueva actividad
        intent.putExtra("origen", "Mi edad es de: ");
        intent.putExtra("miEdad", 30);

        //lanzamos la nueva actividad
        startActivity(intent);

    }
}