package cl.grebolledoa.misactividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class OtherActivity extends AppCompatActivity {

    private TextView tv_parametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

        this.tv_parametro = findViewById(R.id.tv_parametro);

        //obtenemos alguna variable de tipo string contenida en el intent por medio de su name
        String primerParametro = getIntent().getStringExtra("origen");
        //obtenemos alguna variable de tipo entero contenida en el intent por medio de su name
        int miEdad = getIntent().getIntExtra("miEdad", 0);



        this.tv_parametro.setText(primerParametro + miEdad);
    }
}